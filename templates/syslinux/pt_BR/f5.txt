0fPARÂMETROS ESPECIAIS DE INICIALIZAÇÃO - VISÃO GERAL07                                            09F507

Em alguns poucos sistemas, você pode precisar especificar um parâmetro no
prompt 0fboot:07 para que consiga incializar o sistema. Por exemplo, o
Linux pode não estar apto a detectar automaticamente seu hardware, e você
talvez necessite especificar sua localização ou tipo para que seja reconhecido.

Para mais informações sober quais parâmetros de inicialização você pode usar,
pressione:

 <09F607> -- parâmetros de inicialização para máquinas específicas
 <09F707> -- parâmetros de inicialização para controladores de disco diversos
 <09F807> -- parâmetros de inicialização reconhecido pelo sistema de instalação

Note que para especificar um parâmetro para um módulo do kernel em particular, use a forma 
módulo.parametro=valor, por exemplo: libdata.atapi_enabled=1








Pressione F1control e F seguido de 1 para o índice de ajuda, ou ENTER para 
